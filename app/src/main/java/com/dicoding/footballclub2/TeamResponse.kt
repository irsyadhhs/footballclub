package com.dicoding.footballclub2

data class TeamResponse(
        val teams: List<Team>)